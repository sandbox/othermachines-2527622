<?php
/**
 * @file
 * Contains \Drupal\monitoring_updates\Sensor\Sensors\SensorProjectUpdate.
 */

namespace Drupal\monitoring_projects\Sensor\Sensors;

use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\Sensor\SensorConfigurable;

/**
 * Monitors for available updates for a contributed project.
 */
class SensorProjectUpdate extends SensorConfigurable {

  const LIFETIME = 86400;

  /**
   * @var array
   */
  protected static $project_data;

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, &$form_state) {
    $form = parent::settingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $result) {
    if (self::$project_data === NULL) {

      $available = update_get_available();

      // Force update if data is too old. (Thanks, Aron Novak!)
      $needs_update = FALSE;
      foreach ($available as $project) {
        if (REQUEST_TIME - $project['last_fetch'] > SensorProjectUpdate::LIFETIME) {
          $needs_update = TRUE;
          break;
        }
      }
      if ($needs_update) {
        $available = update_get_available(TRUE);
      }

      self::$project_data = update_calculate_project_data($available);
    }
    $this->checkContrib($result);
  }

  /**
   * Checks project status and sets sensor status message.
   *
   * @param SensorResultInterface $result
   */
  protected function checkContrib(SensorResultInterface $result) {
    $info = $this->info->getSetting('info');

    // If the project is available on drupal.org, use the information returned;
    // otherwise set status to UPDATE_UNKNOWN.
    if (isset(self::$project_data[$info['name']])) {
      $info = self::$project_data[$info['name']];
    }
    else {
      $info = array('status' => UPDATE_UNKNOWN);
    }

    $status = $this->getStatusText($info['status']);

    switch ($status) {
      case 'current':
        $result->setStatus(SensorResultInterface::STATUS_OK);
        $result->addStatusMessage('up to date');
        break;

      case 'unknown':
        $result->setStatus(SensorResultInterface::STATUS_INFO);
        $result->addStatusMessage('no releases found');
        break;

      default:

        $critical = $info['status'] == UPDATE_NOT_SECURE || $info['status'] == UPDATE_NOT_SUPPORTED;

        $result->setStatus($critical ? SensorResultInterface::STATUS_CRITICAL : SensorResultInterface::STATUS_INFO);

        $result->addStatusMessage('@status (@current) - recommended @recommended - latest @latest', array(
          '@status' => $status,
          '@current' => isset($info['existing_version']) ? $info['existing_version'] : NULL,
          '@recommended' => isset($info['recommended']) ? $info['recommended'] : NULL,
          '@latest' => isset($info['latest_version']) ? $info['latest_version'] : NULL,
        ));
    }
  }

  /**
   * Gets status text.
   *
   * @param int $status
   *   One of UPDATE_* constants.
   *
   * @return string
   *   Status text.
   */
  protected function getStatusText($status) {
    switch ($status) {
      case UPDATE_NOT_SECURE:
        return 'NOT SECURE';
        break;

      case UPDATE_CURRENT:
        return 'current';
        break;

      case UPDATE_REVOKED:
        return 'version revoked';
        break;

      case UPDATE_NOT_SUPPORTED:
        return 'not supported';
        break;

      case UPDATE_NOT_CURRENT:
        return 'update available';
        break;

      case UPDATE_UNKNOWN:
      case UPDATE_NOT_CHECKED:
      case UPDATE_NOT_FETCHED:
      case UPDATE_FETCH_PENDING:
        return 'unknown';
        break;
    }
  }
}
