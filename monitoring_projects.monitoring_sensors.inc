<?php
/**
 * @file
 * Define sensors.
 *
 * @see hook_monitoring_sensor_info()
 */

/**
 * Implements hook_monitoring_sensor_info().
 */
function monitoring_projects_monitoring_sensor_info() {

  $sensors = array();

  foreach (system_list('module_enabled') + system_list('theme') as $name => $project) {

    // Skip disabled projects.
    if (!$project->status) {
      continue;
    }

    // Skip core projects.
    if (isset($project->info['project']) && $project->info['project'] == 'drupal') {
      continue;
    }

    // Skip submodules.
    if (isset($project->info['project']) && $project->info['project'] != $name) {
      continue;
    }

    $sensors['update_project_' . $name] = array(
      'label' => format_string('@name project', array('@name' => $project->info['name'])),
      'description' => format_string('Available updates for the @name project', array('@name' => $project->info['name'])),
      'sensor_class' => 'Drupal\monitoring_projects\Sensor\Sensors\SensorProjectUpdate',
      'numeric' => FALSE,
      'settings' => array(
        'enabled' => TRUE,
        'category' => 'Update',
        'caching_time' => Drupal\monitoring_projects\Sensor\Sensors\SensorProjectUpdate::LIFETIME,
        'info' => (array) $project,
      ),
    );
  }
  return $sensors;
}

/**
 * Implements hook_monitoring_sensor_info_alter().
 */
function monitoring_projects_monitoring_sensor_info_alter(&$info) {

  // Disable "Modules and themes" sensor.
  $info['update_contrib']['settings']['enabled'] = FALSE;
}